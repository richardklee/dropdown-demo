import preprocess from 'svelte-preprocess';
import adapter from '@sveltejs/adapter-auto';
import path from 'path';
import { mdsvex } from "mdsvex"

/** @type {import('@sveltejs/kit').Config} */
const config = {
	kit: {
		adapter: adapter(),
		vite: {
			resolve: {
				alias: {
					$components: path.resolve('./src/components'),
				}
			},
			// plugins: [
			// 	svelte({ hot: !process.env.VITEST }),
			// ],
			// test: {
			// 	globals: true,
			// 	environment: 'jsdom',
			// },
		}
	},
	extensions: [".svelte", ".svx", ".md"],
	preprocess: [
		mdsvex({ extensions: ['.svelte.md', '.md', '.svx'] }),
		preprocess({
			postcss: true
		})
	],
};

export default config;
