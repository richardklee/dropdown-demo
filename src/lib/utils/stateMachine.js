// imports between these debug comments will be stripped from production builds
/*START.DEBUG_ONLY*/
import 'robot3/debug';
import 'robot3/logging';
/*END.DEBUG_ONLY*/

import { createMachine, interpret, state, transition, invoke, reduce, action, guard, immediate } from 'robot3';
import { writable, get } from 'svelte/store';

// constructor function to interpret the machine and wrap
// the FSM service into a custom svelte stores.
const useMachine = (machine) => {
    const state = writable(machine.current);
    const context = writable(machine.context());

    // every time the state changes we will update our Svelte stores
    const service = interpret(machine, (service) => {
        state.set(service.machine.current);
        context.set(service.context);
    });

    return [
        { subscribe: state.subscribe },
        { subscribe: context.subscribe, set: context.set },
        service.send
    ];
};

// create our Robot FSM definition
const machineFactory = (machine, initial = {}) => {
    let newMachine;
    let initialContext;

    initialContext = initial;
    newMachine = createMachine(machine, () => initialContext);

    return useMachine(newMachine);
}

export {
    machineFactory,
    state,
    state as final,
    transition,
    guard,
    reduce,
    action,
    immediate,
    invoke,
}