import _debounce from "lodash.debounce";

export const debounce = _debounce;

/**
 * Calculates the percentage of (amount / total) and returns a truncated integer
 * @param {Number} amount
 * @param {Number} total
 * @returns an integer of zero if the input is invalid, else the percentage as a truncated integer
 */
export const calcPercentage = (amount, total) => {
    try {
        if (
            total === 0 ||
            total === null ||
            total === undefined ||
            amount === undefined ||
            Number.isNaN(amount) ||
            Number.isNaN(total)
        ) {
            return 0;
        }
        return Math.trunc(Number((amount / total) * 100));
    } catch (e) {
        console.error(e);
        return 0;
    }
};

/**
 * ** Current implmentation only abbreviates positive numbers **
 *
 * Abbreviates the given value.
 * I.e. 1500 => "1.5k", or 2500000 => "2.5m"
 *
 * @param {Number} n
 * @returns null if n is not zero and falsy, else returns the abbreviated number string
 */
export const formatNumberAbbrevStr = (n) => {
    if (!n && n !== 0) {
        return null;
    }

    const suffixes = ["", "K", "M", "B", "T"];

    let newValue = n;
    let suffixNum = 0;
    while (newValue >= 1000) {
        newValue /= 1000;
        suffixNum++;
    }

    const precision = newValue.toString().length === 4 ? 2 : 3;
    newValue = +newValue.toPrecision(precision);

    return `${newValue}${suffixes[suffixNum]}`;
};

/**
 * Function to return the material icon for the supplied content type
 *
 * @param {String} type
 * @returns Material icon string
 */
export const icons = type => {
    const iconMap = {
      admin: 'mdi mdi-tune',
      article: 'mdi mdi-text-box',
      checklist: 'mdi mdi-format-list-checks',
      checklisttemplate: 'mdi mdi-format-list-checks',
      content: 'mdi mdi-text',
      dashboard: 'mdi mdi-chart-line-variant',
      home: 'mdi mdi-home',
      jobfunction: 'mdi mdi-format-list-bulleted',
      workfunction: 'mdi mdi-format-list-bulleted',
      mediamanager: 'mdi mdi-folder-multiple-image',
      moment: 'mdi mdi-clock-outline',
      object: 'mdi mdi-toolbox',//'mdi mdi-briefcase-variant',
      profile: 'mdi mdi-card-text',
      template: 'mdi mdi-folder-text',
      shared: 'mdi mdi-folder-account',
      survey: 'mdi mdi-forum',
      user: 'mdi mdi-account-supervisor',
      feedback: 'mdi mdi-message-alert'
    }
    return iconMap[type];
  }

export const sleep = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export const clamp = (num, min, max) => {
    return Math.min(Math.max(num, min), max);
}