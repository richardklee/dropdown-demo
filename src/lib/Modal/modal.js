import ModalWrapper from './ModalWrapper.svelte';

export const openModal = config => {

  console.log('FROM MODAL', config)
  let modalOptions = {
    animateHeightChanges: false,
    closeOnOutsideClick: true,
    closeWithEscape: true,
    showCloseButton: true,
    title: '',
    target: document.body,
    width: '400px'
  };

  if (config.options) {
    modalOptions = { ...modalOptions, ...config.options };
  }

  const props = {
    component: config.component,
    options: modalOptions
  }

  if (config.props) {
    props.componentProps = config.props;
  }

  const modal = new ModalWrapper({
    target: modalOptions.target,
    props,
    intro: true
  })

  modal.$on('close', () => {
    modal.$destroy();
  });

  return modal;
}
