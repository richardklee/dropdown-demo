// Deprecated - Should use the openModal method to open a provided component in a modal
import Modal from './Modal.svelte'

import SimpleModal from './SimpleModal.svelte';
import { openModal } from './modal.js'

export { Modal as default, SimpleModal, openModal };