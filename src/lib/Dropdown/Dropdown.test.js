import { describe, expect, it } from 'vitest';
import { render } from '@testing-library/svelte';
import Dropdown from './Dropdown.svelte';
import { fireEvent } from '@testing-library/svelte';

let OPTIONS = [
  { label: 'test option 1', value: 'test-option-1' },
  { label: 'test option 2', value: 'test-option-2' },
  { label: 'test option 3', value: 'test-option-3' },
  { label: 'test option 3', value: 'test-option-3' }
];


describe("Sample Test Block", () => {
  it("sample test which should be true", () => {

      expect(true).toBe(true);
  })
})

describe("Basic dropdown", () => {
  it("Expect three items to be rendered", async () => {
    const results = render(Dropdown, {
      options: OPTIONS
    })

    const test = results.getByTestId('dropdown-button');
    
    await fireEvent.click(test);
    const test2 = results.getByTestId('dropdown-container').getElementsByClassName('block').length
    
    expect(test2).toBe(OPTIONS.length);
  })
})