import Dropdown from './Dropdown.svelte';
import CheckboxOption from './CheckboxOption.svelte';
import Option from './Option.svelte';

Dropdown.CheckboxOption = CheckboxOption;
Dropdown.Option = Option;

export default Dropdown;