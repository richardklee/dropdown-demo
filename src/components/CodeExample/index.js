import CodeExample from './CodeExample.svelte';
import Container from './CodeExampleContainer.svelte';

CodeExample.Container = Container;

export default CodeExample;