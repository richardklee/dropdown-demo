# Headings

----

# H1
text-5xl font-semibold tracking-tighter leading-tight mb-4

## H2
text-4xl font-semibold tracking-tight leading-tight mb-4

### H3
text-3xl font-semibold tracking-tight leading-tight mb-3

#### H4
text-xl font-semibold tracking-tight leading-tight mb-2

##### H5
text-lg font-semibold tracking-normal leading-snug mb-2