<script>
  import DynamicModal from '$lib/DynamicModal';
  import ComponentExplorer from '$components/ComponentExplorer.svelte';
  import APITable from '$components/APITable.svelte';
  import CodeExample from '$components/CodeExample';
  import Button from '$lib/Button';

  let componentAPI = [
    {
      prop: '',
      description: '',
      type: '',
      default: '',
    },
  ]

  let explorerProps = {
    // prop: {value: '', type: ''},
  }

  // Code for examples
  let openExample = false;
</script>

<style></style>

# Dynamic Modal

### Description

Description of why this is a dynamic modal....????

### Examples 
<CodeExample.Container columns={2}>
  <CodeExample
    title="Example title"
    language="svelte"
    code={`
    
    `}
  >
    <Button on:click={()=>openExample = !openExample}>Open Modal</Button>
  </CodeExample>
</CodeExample.Container>

<DynamicModal open={openExample}>
  <p>Content of modal</p>
</DynamicModal>

### Component Explorer

<ComponentExplorer bind:componentProps={explorerProps}>
   <!-- Place component here -->
</ComponentExplorer>

### API

<APITable data={componentAPI} />