<script>
  // import DynamicModal from '$lib/DynamicModal';
  import ComponentExplorer from '$components/ComponentExplorer.svelte';
  import APITable from '$components/APITable.svelte';
  import CodeExample from '$components/CodeExample';

  let componentAPI = [
    {
      prop: '',
      description: '',
      type: '',
      default: '',
    },
  ]

  let explorerProps = {
    // prop: {value: '', type: ''},
  }

</script>

<style></style>

<CodeExample.Container columns={2}>
  <CodeExample
    title="Example title"
    language="svelte"
    code={`
    
    `}
  >
    
  </CodeExample>
</CodeExample.Container>

### Component Explorer

<ComponentExplorer bind:componentProps={explorerProps}>
   <!-- Place component here -->
</ComponentExplorer>

### API

<APITable data={componentAPI} />