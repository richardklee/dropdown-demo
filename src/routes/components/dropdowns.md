<script>
	import Dropdown from '$lib/Dropdown/';
	import ComponentExplorer from '$components/ComponentExplorer.svelte';
  import APITable from '$components/APITable.svelte';
  import CodeExample from '$components/CodeExample';
  import Button from '$lib/Button';

	let OPTIONS = [
		{ label: 'test option 1', value: 'test-option-1' },
		{ label: 'test option 2', value: 'test-option-2' },
		{ label: 'test option 3', value: 'test-option-3' }
	];

  let explorerProps = {
    showBorder: {value: true, type: 'boolean'},
    placeholder: {value: '', type: 'string'},
    allowSearch: {value: false, type: 'boolean'},
    mode: {value: 'multi', type: ['single', 'multi', 'standard']},
    showBlocks: {value: false, type: 'boolean'},
    blockLimit: {value: 2, type: 'number'},
    allowSelectAll: {value: false, type: 'boolean'},
    selectAllType: {value: "option", type: ["option", "button"]},
    bubbleSelected: {value: false, type: 'boolean'},
    isLoading: { valuse: false, type: 'boolean'}
  }

  let dropdownAPI = [
    {
      prop: 'showBorder',
      description: 'Whether or not to show a border around the field',
      type: 'boolean',
      default: 'true'
    },
    {
      prop: 'placeholder',
      description: 'Define placeholder text',
      type: 'string',
      default: ''
    },
    {
      prop: 'allowSearch',
      description: 'Show search input field',
      type: 'boolean',
      default: 'false'
    },
    {
      prop: 'mode',
      description: 'Define if the mode type of the dropdown should be single|multi',
      type: 'string',
      default: 'single'
    },
    {
      prop: 'showBlocks',
      description: '[Experimental] Option to view selected items as inline blocks',
      type: 'boolean',
      default: 'false'
    },
    {
      prop: 'blockLimit',
      description: '[Experimental] Define the maximum number of blocks to show. If the number of selected is greater than the limit, a "more" block will be shown identifying how many more are selected. Value of -1 is treated as unlimited',
      type: 'number',
      default: '3'
    },
    {
      prop: 'allowSelectAll',
      description: 'Allow option to select all options. Only works with multi-select dropdowns.',
      type: 'boolean',
      default: 'false'
    },
    {
      prop: 'selectAllType',
      description: 'Change the type of select all interaction. The "option" type will display the Select All as an item in the option list.',
      type: 'string [option|button]',
      default: 'option'
    },
    {
      prop: 'bubbleSelected',
      description: 'Bubble selected options to the top of the list.',
      type: 'boolean',
      default: 'false'
    },
    {
      prop: 'customLabel',
      description: 'Custom function to modify label string when items are selected.',
      type: 'function({options, selected})',
      default: ''
    },
    {
      prop: 'sort',
      description: 'Provide a custom sort function',
      type: 'function({options})',
      default: ''
    },
    {
      prop: 'search',
      description: 'Provide a custom search function',
      type: 'function({options, searchProperties})',
      default: ''
    },
    {
      prop: 'searchProperties',
      description: '[Experimental] Default search function will only seach the specified properties in the Options object.',
      type: 'Array',
      default: '[ ]'
    },
    {
      prop: 'labelProperty',
      description: 'Override the default property that is used for the Option label.',
      type: 'String',
      default: 'label'
    },
    {
      prop: 'valueProperty',
      description: 'Override the default property that is used for the Option value.',
      type: 'String',
      default: 'value'
    },
  ]

  let checkboxAPI = [
    {
      prop: 'option',
      description: 'Object of the data for the option. Must contain a label and value property',
      type: 'object',
      default: '{ label: "", value: "" }'
    },
    {
      prop: 'selected',
      description: 'An array of all the selected options for the dropdown',
      type: 'array',
      default: '[]'
    },
    {
      prop: 'disabled',
      description: 'Disable an option',
      type: 'boolean',
      default: '[]'
    },
    {
      prop: 'highlightSelected',
      description: 'Background highlight the selected option',
      type: 'boolean',
      default: 'false'
    },
    {
      prop: 'labelProperty',
      description: 'Override the default property that is used for the Option label.',
      type: 'String',
      default: 'label'
    },
    {
      prop: 'valueProperty',
      description: 'Override the default property that is used for the Option value.',
      type: 'String',
      default: 'value'
    },
  ]

  let exampleOptionComponentType = 'default'

  function addOption() {
    OPTIONS.push({
      label: 'test option ' + (OPTIONS.length + 1),
      value: 'test-option-' + (OPTIONS.length + 1)
    });
  }
</script>

<style>
	.container {
		max-width: 1000px;
	}
</style>

# Dropdown

### Usage

Compile an array of options that will be rendered in the drop down. 

```javascript
  const optionsArray = [
    { label: 'option 1', value: 'option-1' },
    { label: 'option 2', value: 'option-2' },
    { label: 'option 3', value: 'option-3' }
  ]
```

Use the `Dropdown` component to render the dropdown and provide it the `optionsArray`. By default, if no `slot` is provided, `Dropdown` will render a default `Option` component. 
```svelte
  <Dropdown
    options={optionsArray}
    ...optionalProps
  />
```

`Dropdown.CheckboxOption` can be used to render dropdowns with checkboxes. This requires an `option` and `selected` prop which can be populated by the exposed `data` object from `Dropdown`.
```svelte
  <Dropdown
    options={optionsArray}
    ...optionalProps
    let:data      
  >
    <Dropdown.CheckboxOption option={data.option} selected={data.selected} />
  </Dropdown>
```

To customize the label output, use the name slot `buttonLabel`.
```svelte
  <Dropdown
    options={optionsArray}
    ...optionalProps
    let:data      
  >
   <div slot="buttonLabel">
      {#if data.selected.length === 0}
        Select tags
      {:else}
        Tags ({data.selected.length})
      {/if}
    </div>
    <Dropdown.CheckboxOption option={data.option} selected={data.selected} />
  </Dropdown>
```


### Examples 
<CodeExample.Container columns={2}>
  <CodeExample
    title="Basic Dropdown"
    language="svelte"
    description="Dropdown with the minimum requirements"
    code={`
    <Dropdown
      placeholder="Choose an option"
      options={OPTIONS}
      let:data
    />
    `}
  >
    <Dropdown
      placeholder="Choose an option"
      options={OPTIONS}
      let:data
    />
  </CodeExample>

  <CodeExample
    title="Dropdown with multiselect"
    language="svelte"
    code={`
      <Dropdown
        placeholder="Choose an option"
        options={OPTIONS}
        mode="multi"
        showBorder={true}
        let:data
      >
        <Dropdown.Option option={data.option} selected={data.selected}/>
      </Dropdown>
    `}
  >
    <Dropdown
      placeholder="Choose an option"
      options={OPTIONS}
      mode="multi"
      showBorder={true}
      let:data
    >
      <Dropdown.Option option={data.option} selected={data.selected}/>
    </Dropdown>
  </CodeExample>

  <CodeExample
    title="Dropdown with search & list output"
    language="svelte"
    code={`
      <Dropdown
        placeholder="Choose an option"
        options={OPTIONS}
        mode="multi"
        showBorder={true}
        allowSearch={true}
        let:data
      >
        <Dropdown.Option option={data.option} selected={data.selected}/>
      </Dropdown>
    `}
  >
    <Dropdown
      placeholder="Choose an option"
      options={OPTIONS}
      mode="multi"
      showBorder={true}
      allowSearch={true}
      let:data
    >
      <Dropdown.Option option={data.option} selected={data.selected}/>
    </Dropdown>
  </CodeExample>

  <CodeExample
    title="Dropdown with search & list output with select all"
    language="svelte"
    code={`
      <Dropdown
        placeholder="Choose an option"
        options={OPTIONS}
        mode="multi"
        showBorder={true}
        allowSearch={true}
        allowSelectAll={true}
        let:data
      >
        <Dropdown.Option option={data.option} selected={data.selected}/>
      </Dropdown>
    `}
  >
    <Dropdown
      placeholder="Choose an option"
      options={OPTIONS}
      mode="multi"
      showBorder={true}
      allowSearch={true}
      allowSelectAll={true}
      let:data
    >
      <Dropdown.Option option={data.option} selected={data.selected}/>
    </Dropdown>
  </CodeExample>
  
  <CodeExample
    title="Dropdown with search"
    language="svelte"
    code={`
      <Dropdown
        placeholder="Choose an option"
        options={OPTIONS}
        mode="multi"
        showBorder={true}
        allowSearch={true}
        showBlocks={false}
        let:data
      >
        <Dropdown.Option option={data.option} selected={data.selected}/>
      </Dropdown>
    `}
  >
    <Dropdown
      placeholder="Choose an option"
      options={OPTIONS}
      mode="multi"
      showBorder={true}
      allowSearch={true}
      showBlocks={false}
      let:data
    >
      <Dropdown.Option option={data.option} selected={data.selected}/>
    </Dropdown>
  </CodeExample>

  <CodeExample
    title="Dropdown with checkbox options"
    language="svelte"
    code={`
      <Dropdown
        placeholder="Choose an option"
        options={OPTIONS}
        mode="multi"
        showBorder={true}
        allowSearch={true}
        showBlocks={true}
        let:data
      >
        <Dropdown.CheckboxOption option={data.option} selected={data.selected}/>
      </Dropdown>
    `}
  >
    <Dropdown
      placeholder="Choose an option"
      options={OPTIONS}
      mode="multi"
      showBorder={true}
      allowSearch={true}
      showBlocks={true}
      let:data
    >
      <Dropdown.CheckboxOption option={data.option} selected={data.selected}/>
    </Dropdown>
  </CodeExample>

  <CodeExample
    title="Dropdown with checkbox options with highlighted rows"
    language="svelte"
    code={`
      <Dropdown
        placeholder="Choose an option"
        options={OPTIONS}
        mode="multi"
        showBorder={true}
        allowSearch={true}
        showBlocks={true}
        let:data
      >
        <Dropdown.CheckboxOption option={data.option} selected={data.selected} highlightSelected={true}/>
      </Dropdown>
    `}
  >
    <Dropdown
      placeholder="Choose an option"
      options={OPTIONS}
      mode="multi"
      showBorder={true}
      allowSearch={true}
      showBlocks={false}
      let:data
    >
      <Dropdown.CheckboxOption option={data.option} selected={data.selected} highlightSelected={true}/>
    </Dropdown>
  </CodeExample>
  
  <CodeExample
    title="Dropdown with checkbox options with select all"
    language="svelte"
    code={`
      <Dropdown
        placeholder="Choose an option"
        options={OPTIONS}
        mode="multi"
        showBorder={true}
        allowSearch={true}
        showBlocks={false}
        allowSelectAll={true}
        let:data
      >
        <Dropdown.CheckboxOption option={data.option} selected={data.selected}/>
      </Dropdown>
    `}
  >  
    <Dropdown
      placeholder="Choose an option"
      options={OPTIONS}
      mode="multi"
      showBorder={true}
      allowSearch={true}
      showBlocks={false}
      allowSelectAll={true}
      let:data
    >
      <Dropdown.CheckboxOption option={data.option} selected={data.selected}/>
    </Dropdown>
  </CodeExample>

  <CodeExample
    title="Dropdown with checkbox options with select all as an Option"
    language="svelte"
    code={`
      <Dropdown
      placeholder="Choose an option"
      options={OPTIONS}
      mode="multi"
      showBorder={true}
      allowSearch={true}
      showBlocks={false}
      allowSelectAll={true}
      selectAllType="option"
      let:data
    >
    </Dropdown>
    `}
  >  
    <Dropdown
      placeholder="Choose an option"
      options={OPTIONS}
      mode="multi"
      showBorder={true}
      allowSearch={true}
      showBlocks={false}
      allowSelectAll={true}
      selectAllType="option"
      let:data
    >
    </Dropdown>
  </CodeExample>

  <CodeExample
    title="Dropdown with checkbox options with select all as an Option (checkboxes)"
    language="svelte"
    code={`
      <Dropdown
        placeholder="Choose an option"
        options={OPTIONS}
        mode="multi"
        showBorder={true}
        allowSearch={true}
        showBlocks={false}
        allowSelectAll={true}
        selectAllType="option"
        let:data
      >
        <Dropdown.CheckboxOption option={{label: 'All items'}} selected={data.selected} slot="customSelectAll"/>
        <Dropdown.CheckboxOption {...data}/>
      </Dropdown>
    `}
  >  
    <Dropdown
      placeholder="Choose an option"
      options={OPTIONS}
      mode="multi"
      showBorder={true}
      allowSearch={true}
      showBlocks={false}
      allowSelectAll={true}
      selectAllType="option"
      let:data
    >
      <Dropdown.CheckboxOption option={{label: 'All items'}} selected={data.selected} slot="customSelectAll"/>
      <Dropdown.CheckboxOption {...data}/>
    </Dropdown>
  </CodeExample>

  <CodeExample
    title="Dropdown with custom label"
    language="svelte"
    code={`
      <Dropdown
        placeholder="Choose an option"
        options={OPTIONS}
        mode="multi"
        showBorder={true}
        allowSearch={true}
        showBlocks={false}
        allowSelectAll={true}
        customLabel={
          ({selected}) => {
            if (selected.length === 0) {
              return 'Select tags'
            } else {
              return 'test'
            }
          }
        }
        let:data
      >
        <Dropdown.CheckboxOption option={data.option} selected={data.selected}/>
      </Dropdown>
    `}
  >
    <Dropdown
      placeholder="Choose an option"
      options={OPTIONS}
      mode="multi"
      showBorder={true}
      allowSearch={true}
      showBlocks={false}
      allowSelectAll={true}
      customLabel={
          ({selected}) => {
            if (selected.length === 0) {
              return 'Select tags'
            } else {
              return `Tags (${selected.length})`
            }
          }
        }
      let:data
    >
      <Dropdown.CheckboxOption option={data.option} selected={data.selected}/>
    </Dropdown>
  </CodeExample>

  <CodeExample
    title="Dropdown with custom label slot"
    language="svelte"
    code={`
      <Dropdown
        placeholder="Choose an option"
        options={OPTIONS}
        mode="multi"
        showBorder={true}
        allowSearch={true}
        showBlocks={false}
        allowSelectAll={true}
        let:data
      >
        <div slot="buttonLabel">
          {#if data.selected.length === 0}
            Select tags
          {:else}
            Tags ({data.selected.length})
          {/if}
        </div>
        <Dropdown.CheckboxOption option={data.option} selected={data.selected}/>
      </Dropdown>
    `}
  >
    <Dropdown
      placeholder="Choose an option"
      options={OPTIONS}
      mode="multi"
      showBorder={true}
      allowSearch={true}
      showBlocks={false}
      allowSelectAll={true}
      let:data
    >
      <div slot="buttonLabel">
        {#if data.selected.length === 0}
          Select tags
        {:else}
          Tags ({data.selected.length})
        {/if}
      </div>
      <Dropdown.CheckboxOption option={data.option} selected={data.selected}/>
    </Dropdown>
  </CodeExample>

  <CodeExample
    title="Dropdown in set width"
    language="svelte"
    code={`
      <div class="flex">
        <div class="w-48">
          <Dropdown
            placeholder="Choose an option"
            options={OPTIONS}
            mode="multi"
            showBorder={true}
            allowSearch={true}
            showBlocks={false}
            allowSelectAll={true}
            let:data 
          >
            <div slot="buttonLabel">
              {#if data.selected.length === 0}
                Select tags
              {:else}
                Tags ({data.selected.length})
              {/if}
            </div>
            <Dropdown.CheckboxOption option={data.option} selected={data.selected}/>
          </Dropdown>
        </div>
      
        <Dropdown
          placeholder="Choose an option"
          options={OPTIONS}
          mode="multi"
          showBorder={true}
          allowSearch={true}
          showBlocks={false}
          allowSelectAll={true}
          class="w-48"
          let:data
        >
          <div slot="buttonLabel">
            {#if data.selected.length === 0}
              Select keywords
            {:else}
              Keywords ({data.selected.length})
            {/if}
          </div>
          <Dropdown.CheckboxOption option={data.option} selected={data.selected}/>
        </Dropdown>
      </div>
    `}
  >
  <div class="flex">
      <div class="w-48">
        <Dropdown
          placeholder="Choose an option"
          options={OPTIONS}
          mode="multi"
          showBorder={true}
          allowSearch={true}
          showBlocks={false}
          allowSelectAll={true}
          let:data 
        >
          <div slot="buttonLabel">
            {#if data.selected.length === 0}
              Select tags
            {:else}
              Tags ({data.selected.length})
            {/if}
          </div>
          <Dropdown.CheckboxOption option={data.option} selected={data.selected}/>
        </Dropdown>
      </div>
      <Dropdown
        placeholder="Choose an option"
        options={OPTIONS}
        mode="multi"
        showBorder={true}
        allowSearch={true}
        showBlocks={false}
        allowSelectAll={true}
        class="w-48"
        let:data
      >
        <div slot="buttonLabel">
          {#if data.selected.length === 0}
            Select keywords
          {:else}
            Keywords ({data.selected.length})
          {/if}
        </div>
        <Dropdown.CheckboxOption option={data.option} selected={data.selected}/>
      </Dropdown>
    </div>
  </CodeExample>

	<CodeExample
    title="Bubble Selected"
    language="svelte"
    code={`
      <Dropdown
        placeholder="Choose an option"
        options={OPTIONS}
        mode="multi"
        showBorder={true}
        allowSearch={true}
        showBlocks={false}
        allowSelectAll={true}
        bubbleSelected={true}
        let:data
      >
        <div slot="buttonLabel">
          {#if data.selected.length === 0}
            Select tags
          {:else}
            Tags ({data.selected.length})
          {/if}
        </div>
        <Dropdown.CheckboxOption option={data.option} selected={data.selected}/>
      </Dropdown>
    `}
  >
    <Dropdown
      placeholder="Choose an option"
      options={OPTIONS}
      mode="multi"
      showBorder={true}
      allowSearch={true}
      showBlocks={false}
      allowSelectAll={true}
      bubbleSelected={true}
      let:data
    >
      <div slot="buttonLabel">
        {#if data.selected.length === 0}
          Select tags
        {:else}
          Tags ({data.selected.length})
        {/if}
      </div>
      <Dropdown.CheckboxOption option={data.option} selected={data.selected}/>
    </Dropdown>
  </CodeExample>

  <CodeExample
    title="Custom Sort"
    language="svelte"
    code={`
      <Dropdown
        placeholder="Choose an option"
        options={[
          { label: 'test option 1', value: 'test-option-1' },
          { label: 'test option 3', value: 'test-option-3' },
          { label: 'test option 2', value: 'test-option-2' },
        ]}
        mode="multi"
        showBorder={true}
        allowSearch={true}
        showBlocks={false}
        allowSelectAll={true}
        bubbleSelected={true}
        sort={({options})=>{
          const sortedOptions = options.sort((a, b) => {
            if (a.label < b.label) {
              return -1
            } else {
              return 1
            }
          })
          return sortedOptions
        }}
        let:data
      >
        <div slot="buttonLabel">
          {#if data.selected.length === 0}
            Select tags
          {:else}
            Tags ({data.selected.length})
          {/if}
        </div>
        <Dropdown.CheckboxOption option={data.option} selected={data.selected}/>
      </Dropdown>
    `}
  >
    <Dropdown
      placeholder="Choose an option"
      options={[
        { label: 'test option 1', value: 'test-option-1' },
        { label: 'test option 3', value: 'test-option-3' },
        { label: 'test option 2', value: 'test-option-2' },
      ]}
      mode="multi"
      showBorder={true}
      allowSearch={true}
      showBlocks={false}
      allowSelectAll={true}
      bubbleSelected={true}
      sort={({options})=>{
        const sortedOptions = options.sort((a, b) => {
          if (a.label < b.label) {
            return -1
          } else {
            return 1
          }
        })
        return sortedOptions
      }}
      let:data
    >
      <div slot="buttonLabel">
        {#if data.selected.length === 0}
          Select tags
        {:else}
          Tags ({data.selected.length})
        {/if}
      </div>
      <Dropdown.CheckboxOption option={data.option} selected={data.selected}/>
    </Dropdown>
  </CodeExample>

  <CodeExample
    title="Dropdown with overrides to modify label and value source"
    language="svelte"
    code={`
      <Dropdown
        placeholder="Choose an option"
        options={[
          { label: 'test option 1', value: 'test-option-1', name: 'Option 1' },
          { label: 'test option 3', value: 'test-option-3', name: 'Option 2' },
          { label: 'test option 2', value: 'test-option-2', name: 'Option 3' },
        ]}
        mode="multi"
        showBorder={true}
        allowSearch={true}
        showBlocks={false}
        allowSelectAll={true}
        bubbleSelected={true}
        sort={({options})=>{
          const sortedOptions = options.sort((a, b) => {
            if (a.label < b.label) {
              return -1
            } else {
              return 1
            }
          })
          return sortedOptions
        }}
        on:selection={(e)=>{
          console.log('Selection made', e);
        }}
        let:data
      >
        <div slot="buttonLabel">
          {#if data.selected.length === 0}
            Select tags
          {:else}
            Tags ({data.selected.length})
          {/if}
        </div>
        <Dropdown.CheckboxOption option={data.option} selected={data.selected}/>
      </Dropdown>
    `}
  >
    <Dropdown
      placeholder="Choose an option"
      options={[
        { label: 'test option 1', value: 'test-option-1', name: 'Option 1', cuid: 1 },
        { label: 'test option 2', value: 'test-option-2', name: 'Option 2', cuid: 2 },
        { label: 'test option 3', value: 'test-option-3', name: 'Option 3', cuid: 3 },
      ]}
      mode="multi"
      showBorder={true}
      allowSearch={true}
      showBlocks={false}
      allowSelectAll={true}
      bubbleSelected={true}
      labelProperty="name"
      valueProperty="value"
      sort={({options})=>{
        const sortedOptions = options.sort((a, b) => {
          if (a.label < b.label) {
            return -1
          } else {
            return 1
          }
        })
        return sortedOptions
      }}
      on:selection={(e)=>{
        console.log('Selections', e.detail);
      }}
      let:data
    >
      <div slot="buttonLabel">
        {#if data.selected.length === 0}
          Select tags
        {:else}
          Tags ({data.selected.length})
        {/if}
      </div>
      <Dropdown.CheckboxOption {...data} />
    </Dropdown>
  </CodeExample>

  <CodeExample
    title="Show selected as blocks"
    description="(Experimental) Do not use in production yet"
    language="svelte"
    code={`
      <Dropdown
        placeholder="Choose an option"
        options={OPTIONS}
        mode="multi"
        showBorder={true}
        allowSearch={true}
        showBlocks={true}
        allowSelectAll={true}
        bubbleSelected={true}
        let:data
      >
        <Dropdown.CheckboxOption option={data.option} selected={data.selected}/>
      </Dropdown>
    `}
  >
    <Dropdown
      placeholder="Choose an option"
      options={OPTIONS}
      mode="multi"
      showBorder={true}
      allowSearch={true}
      showBlocks={true}
      allowSelectAll={true}
      bubbleSelected={true}
      let:data
    >
      <Dropdown.CheckboxOption option={data.option} selected={data.selected}/>
    </Dropdown>
  </CodeExample>

  <CodeExample
    title="Dropdown with search based on defined properties"
    description="(Experimental) Define properties to search in option object. For this example, test by searching for either red, green, or blue."
    language="svelte"
    code={`
      <Dropdown
        placeholder="Choose an option"
        options={[
          {label: 'Option 1', value: "option-1", colour: 'red'},
          {label: 'Option 2', value: "option-2", colour: 'green'},
          {label: 'Option 3', value: "option-3", colour: 'blue'},
        ]}
        mode="multi"
        showBorder={true}
        allowSearch={true}
        showBlocks={true}
        allowSelectAll={true}
        searchProperties={['colour']}
        let:data
      >
        <Dropdown.CheckboxOption option={data.option} selected={data.selected}/>
      </Dropdown>
    `}
  >
    <Dropdown
      placeholder="Choose an option"
      options={[
          {label: 'Option 1', value: "option-1", colour: 'red'},
          {label: 'Option 2', value: "option-2", colour: 'green'},
          {label: 'Option 3', value: "option-3", colour: 'blue'},
        ]}
      mode="multi"
      showBorder={true}
      allowSearch={true}
      showBlocks={true}
      allowSelectAll={true}
      searchProperties={['colour']}
      let:data
    >
      <Dropdown.CheckboxOption option={data.option} selected={data.selected}/>
    </Dropdown>
  </CodeExample>
</CodeExample.Container>

### Component Explorer
<div class="section">
  <ComponentExplorer bind:componentProps={explorerProps}>
    <Dropdown
      options={OPTIONS}
      mode={explorerProps.mode.value}
      showBorder={explorerProps.showBorder.value}
      placeholder={explorerProps.placeholder.value}
      allowSearch={explorerProps.allowSearch.value}
      showBlocks={explorerProps.showBlocks.value}
      blockLimit={explorerProps.blockLimit.value}
      allowSelectAll={explorerProps.allowSelectAll.value}
      selectAllType={explorerProps.selectAllType.value}
      bubbleSelected={explorerProps.bubbleSelected.value}
      isLoading={explorerProps.isLoading.value}
      let:data      
    >
      {#if exampleOptionComponentType === 'checkbox' }
        <Dropdown.CheckboxOption {...data} slot="customSelectAll"  />
        <Dropdown.CheckboxOption {...data} />
      {:else}
        <Dropdown.Option {...data} />
      {/if}
    </Dropdown>
    
    <div slot="extra-controls" class="flex flex-col">
      <Button slim={true} on:click={addOption}>Add option to dropdown</Button>
      <div>
        <label>Option Component</label>
        <select class="border border-gray-400 rounded" bind:value={exampleOptionComponentType} >
          <option value="default">Default</option>
          <option value="checkbox">Checkbox Options</option>
        </select>
      </div>
    </div>
  </ComponentExplorer>
</div>

### API

#### Dropdown Component
<APITable data={dropdownAPI}>
</APITable>

#### Dropdown.CheckboxOption Component
<APITable data={checkboxAPI}>
</APITable>