<script>
  import Button from '$lib/Button';
  import ComponentExplorer from '$components/ComponentExplorer.svelte'
  import APITable from '$components/APITable.svelte';
  import CodeExample from '$components/CodeExample';

  let explorerProps = {
    expanded: {value: false, type: 'boolean'},
    slim: {value: false, type: 'boolean'},
    type: {value: 'primary', type: ['primary', 'secondary', 'secondary-alt', 'brand', 'danger', 'success', 'warning', 'text', 'icon']},
    loading: {value: false, type: 'boolean'},
    disabled: {value: false, type: 'boolean'},
  }

  let buttonAPI = [
    {
      prop: 'expanded',
      description: 'Does the button expand to take full width of its container',
      type: 'boolean',
      default: 'false'
    },
    {
      prop: 'slim',
      description: 'Use slim style',
      type: 'boolean',
      default: 'false'
    },
    {
      prop: 'type',
      description: 'Define the type of button',
      type: 'string [primary|secondary|secondary-alt|brand|brand-light|danger|success|warning|text|icon]',
      default: 'primary'
    },
    {
      prop: 'loading',
      description: 'Show loading state',
      type: 'boolean',
      default: 'false'
    },
    {
      prop: 'disabled',
      description: 'Disabled the button',
      type: 'boolean',
      default: 'false'
    },
  ]
</script>

# Buttons

### Examples

<CodeExample.Container columns={2}>
  <CodeExample
    title="Default button"
    code={`<Button>Basic Button</Button>`}
  >
    <Button>Basic Button</Button>
  </CodeExample>
  <CodeExample
    title="Expanded button"
    code={`<Button expanded={true}>Expanded Button</Button>`}
  >
    <Button expanded={true}>Expanded Button</Button>
  </CodeExample>
  <CodeExample
    title="Slim button"
    code={`<Button slim={true}>Button</Button>`}
  >
    <Button slim={true}>Button</Button>
  </CodeExample>
  <CodeExample
    title="Different states"
    code={`
    <Button loading={true}>Button</Button>
    <Button disabled={true}>Disabled Button</Button>
    `}
  >
    <Button loading={true}>Button</Button>
    <Button disabled={true}>Disabled Button</Button>
  </CodeExample>
</CodeExample.Container>

<div class="section">
  <h4>Component Explorer</h4>

  <ComponentExplorer bind:componentProps={explorerProps}>
    <Button
      expanded={explorerProps.expanded.value}
      slim={explorerProps.slim.value}
      type={explorerProps.type.value}
      loading={explorerProps.loading.value}
      disabled={explorerProps.disabled.value}
    >
      Button Label
    </Button>
  </ComponentExplorer>
</div>

### API

#### Dropdown Component
<APITable data={buttonAPI}>
</APITable>