<script>
  import Checkbox from '$lib/Checkbox/Checkbox.svelte';
	import ComponentExplorer from '$components/ComponentExplorer.svelte';
  import APITable from '$components/APITable.svelte';
  import CodeExample from '$components/CodeExample';
  import Button from '$lib/Button';

  let explorerProps = {
    value: { value: false, type: 'boolean' },
    nativeValue: { value: false, type: 'boolean' },
    disabled: { value: false, type: 'boolean' },
  }

  let checkboxAPI = [
    {
      prop: '',
      description: '',
      type: '',
      default: ''
    }
  ]

  // Example code
  let exampleValue = true;
  let exampleArray = ['Item 1', 'Item 2', 'Item 3']
  let exampleSelected = [];
</script>

# Checkbox

### Examples

<CodeExample.Container columns={2}>
  <CodeExample
    title="Default Checkbox"
    language="svelte"
    code={`
    <Checkbox
      bind:value={exampleValue}
      on:change={(e)=>console.log('Hello', {
        exampleValue,
        detail: e.detail
      })}
    >
      Label
    </Checkbox>
    `}
  >
    <Checkbox
      bind:value={exampleValue}
      on:change={(e)=>console.log('Hello', {
        exampleValue,
        detail: e.detail
      })}
    >
      Label
    </Checkbox>
  </CodeExample>
  <CodeExample
    title="Checkbox Static States"
    language="svelte"
    code={`
      <Checkbox
        disabled={true}
        on:change={()=>console.log('test')}
      >
        Disabled
      </Checkbox>
      <Checkbox
        disabled={false}
        on:change={()=>console.log('test')}
      >
        Enabled
      </Checkbox>
    `}
  >
    <Checkbox
      disabled={true}
      on:change={()=>{}}
    >
      Disabled
    </Checkbox>
    <Checkbox
      disabled={false}
      on:change={()=>{}}
    >
      Enabled
    </Checkbox>
  </CodeExample>

  <CodeExample
    title="Checkbox with arrays"
    language="svelte"
    code={`
      
    `}
  >
    {#each exampleArray as item}
      <Checkbox
        bind:value={exampleSelected}
        nativeValue={item}
        on:change={(e)=>console.log({ exampleArray, exampleSelected, selected: e.detail })}
      >
        {item}
      </Checkbox>
    {/each}
  </CodeExample>
</CodeExample.Container>