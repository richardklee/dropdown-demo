<script>
  import TextInput from '$lib/TextInput';
  import ComponentExplorer from '$components/ComponentExplorer.svelte';
  import APITable from '$components/APITable.svelte';
  import CodeExample from '$components/CodeExample';

  let componentAPI = [
    {
      prop: 'value',
      description: 'Value to bind to',
      type: 'string',
      default: '',
    },
    {
      prop: 'icon',
      description: 'Icon to display on left of text input',
      type: 'String',
      default: '',
    },
    {
      prop: 'iconRight',
      description: 'Icon to display on the right',
      type: 'String',
      default: '',
    },
    {
      prop: 'iconClickable',
      description: 'Toggle if icon is clickable',
      type: 'boolean',
      default: 'false',
    },
    {
      prop: 'iconRightClickable',
      description: 'Toggle if icon on the right is clickable',
      type: 'boolean',
      default: 'false',
    },
    {
      prop: '',
      description: '',
      type: '',
      default: '',
    },
  ]

  let explorerProps = {
    // prop: {value: '', type: ''},
  }

</script>

<style></style>

<CodeExample.Container columns={2}>
  <CodeExample
    title="Example title"
    language="svelte"
    code={`
    
    `}
  >
    <TextInput />
  </CodeExample>
</CodeExample.Container>

### Component Explorer

<ComponentExplorer bind:componentProps={explorerProps}>
   <!-- Place component here -->
</ComponentExplorer>

### API

<APITable data={componentAPI} />