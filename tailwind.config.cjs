const defaultTheme = require('tailwindcss/defaultTheme');

const config = {
	content: ['./src/**/*.{html,js,svelte,ts}'],

	theme: {
		extend: {
			textColor: {
				primary: "#FB5045",
			},
			borderColor: {
				primary: {
					50: "#fad9e6",
					100: "#ffd4d1",
					200: "#ffb7b2",
					300: "#ff9790",
					400: "#fd6e65",
					500: "#FB5045",
					600: "#f93f33",
					700: "#f92f22",
					800: "#f92f22",
					900: "#3c0118",
				}
			},
			borderWidth: {
				3: '3px'
			},
			boxShadow: {
				around: '0px 2px 4px 0px rgba(0, 0, 0, 0.05), 0px 4px 12px 0px rgba(0, 0, 0, 0.15)'
			},
			colors: {
				coal: {
					50: '#f9f9f9',
					100: '#f7f7f7',
					200: '#eaeaea',
					300: '#dbdbdb',
					400: '#b2b2b2',
					500: '#7f7f7f',
					600: '#636363',
					700: '#515151',
					800: '#3f3f3f',
					900: '#2d2d2d'
				},
				primary: {
					50: "#fad9e6",
					100: "#ffd4d1",
					200: "#ffb7b2",
					300: "#ff9790",
					400: "#fd6e65",
					500: "#FB5045",
					600: "#f93f33",
					700: "#f92f22",
					800: "#f92f22",
					900: "#3c0118",
				},
				gray: {
					100: "#f3f4f6",
					200: "#edf2f7",
					300: "#e2e8f0",
					400: "#cbd5e0",
					500: "#a0aec0",
					600: "#718096",
					700: "#4a5568",
					800: "#2d3748",
					900: "#1a202c"
				},
			},
			fontFamily: {
				sans: ['Inter var', ...defaultTheme.fontFamily.sans]
			},
			screens: {
				xs: '420px',
				dark: {
					raw: '(prefers-color-scheme: dark)'
				},
				light: {
					raw: '(prefers-color-scheme: light)'
				}
			},
			zIndex: {
				1: '1',
				2: '2',
				'-10': '-10'
			}
		},
		opacity: {
			0: '0',
			25: '.25',
			50: '.5',
			75: '.75',
			10: '.1',
			20: '.2',
			30: '.3',
			40: '.4',
			50: '.5',
			60: '.6',
			70: '.7',
			80: '.8',
			90: '.9',
			100: '1'
		}
	},
	variants: {
		boxShadow: ['group-focus', 'focus'],
		borderWidth: ['responsive', 'last', 'hover', 'focus', 'first'],
		borderRadius: ['last', 'first'],
		background: ['responsive', 'last', 'hover', 'focus', 'first', 'disabled'],
		opacity: ['responsive', 'hover', 'focus', 'disabled']
	},

	plugins: [],
};

module.exports = config;
